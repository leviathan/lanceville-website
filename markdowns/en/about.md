**David Lanzendörfer**

![David Lanzendörfer](/img/david.png)

David is the founder of Lanceville Technology, and his last name is the inspiration for the company name which is his last name translated into English.

Indeed even the company logo is a calligraphy of his name translated into Chinese.

He has many years of experience with designing, producing and testing various types of electronic devices, and even designed his own semiconductor manufacturing process and then manufactured transistors in a laboratory by himself. 

