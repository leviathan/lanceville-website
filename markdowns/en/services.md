Our company offers a wide range of services, of which the categories are listed below:  

- **Localizing applications**  
Because of our multilingual team, we can translate your user interface into many languages:
    + German
    + English
    + Chinese
- **Embedded engineering**  
Due to the many years of experience of our team, we can help you implement your application specific embedded operating system on your custom hardware.
- **Schematics and PCB layout design and manufacturing**  
Due to our many years of experience in this field, we can not only design you schematics but also layout them using our expertise
    + **Schematics design**  
    Due to our many years of experience with [KiCAD](https://www.kicad.org), we can design you a schematic, using a FOSS tool, which is actively maintained and developed by CERN, which has the advantage that you can share it in your company without having to worry about paying an extra license for every installation of a CAD tool.
    + **Layouting**  
    The layouting features of [KiCAD](https://www.kicad.org) have become very impressive over the years, thanks to the great work of CERN, so the layout will be drawn also using the KiCAD tools and then sent to the manufacturer in the Gerber format after the review by our customers.
    * **Manufacturing**  
    Due to our networking in the Portuguese and Chinese industry, we're able to negotiate competitive prices for manufacturing while ensuring good quality of the final product.
    * **Assembly and testing**  
    Assembly and testing is also a service we provide.
- **VLSI Design**  
Thanks to our many years of experience gathered while working on our free and open source semiconductor manufacturing process [LibreSilicon](https://libresilicon.com) we can offer you support all the way from your project idea towards your chip on a PCB:  
    + **Digital design**  
    Our developers are experienced with the use of Verilog and the FOSS tool [OpenLane](https://github.com/The-OpenROAD-Project/OpenLane), which already has produced plenty of silicon verified designs and stood the test of time.
    + **Analog design**  
    Using [Magic](http://opencircuitdesign.com/magic), [KLayout](https://www.klayout.de), [gEDA](http://www.geda-project.org) and [ngspice](https://ngspice.sourceforge.io) we can design and simulate VLSI analog designs for you.
    + **Tapeout**  
    Due to our network in the industry, we are able to help you get access to more affordable shuttle runs for your prototypes through our partners.
    + **Process design and development**  
    Due to our experience with developing LibreSilicon we can assist you with designing, implementing and testing your own new process for your own factory setup, using our test wafer generator [Danube River](https://gitlab.libresilicon.com/generator-tools/danube-river) and generating standard cells for it using [Cell Generator](https://pdk.libresilicon.com).
