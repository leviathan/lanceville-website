**David Lanzendörfer**

![David Lanzendörfer](/img/david.png)

David ist der Gründer von Lanceville Technology und sein Nachname diente als Inspiration für den Firmennamen, der sein ins Englische übersetzter Nachname ist.

Tatsächlich ist sogar das Firmenlogo eine ins Chinesische übersetzte Kalligraphie seines Namens.

Er verfügt über langjährige Erfahrung im Entwerfen, Produzieren und Testen verschiedener Arten elektronischer Geräte und er hat sogar seinen eigenen Halbleiterherstellungsprozess entwickelt und anschließend selbst Transistoren in einem Labor hergestellt.
