Unser Unternehmen bietet eine breite Palette von Dienstleistungen an, von denen die Kategorien unten aufgeführt sind:

- **Anwendungen lokalisieren**  
Aufgrund unseres mehrsprachigen Teams können wir Ihre Benutzeroberfläche in viele Sprachen übersetzen:  
     + Deutsch
     + Englisch
     + Chinesisch
- **Embedded Engineering**  
Aufgrund der langjährigen Erfahrung unseres Teams können wir Sie bei der Implementierung Ihres anwendungsspezifischen Embedded-Betriebssystems auf Ihrer individuellen Hardware unterstützen.
- **Entwurf und Herstellung von Schaltplänen und PCB-Layouts**  
Aufgrund unserer langjährigen Erfahrung auf diesem Gebiet können wir Ihnen nicht nur Schaltpläne entwerfen, sondern diese auch mit unserem Fachwissen gestalten
     + **Schaltplanentwurf**  
     Aufgrund unserer langjährigen Erfahrung mit [KiCAD](https://www.kicad.org) können wir Ihnen mithilfe eines FOSS-Tools, das vom CERN aktiv gepflegt und weiterentwickelt wird, einen Schaltplan entwerfen, was den Vorteil hat, dass Sie diese in Ihrem Unternehmen teilen können, ohne sich Gedanken über die Zahlung einer zusätzlichen Lizenz für jede Installation des CAD-Tools machen zu müssen.
     + **Layout**  
     Die Layoutfunktionen von [KiCAD](https://www.kicad.org) sind im Laufe der Jahre, dank der großartigen Arbeit vom CERN, sehr beeindruckend geworden, sodass auch komplexe Layouts mit den KiCAD-Tools gezeichnet werden können und dann, nach der Überprüfung durch unsere Kunden, im Gerber-Format an den Hersteller geschickt werden können.
     * **Herstellung**  
     Aufgrund unserer Vernetzung in der portugiesischen und chinesischen Industrie sind wir in der Lage, wettbewerbsfähige Preise für die Herstellung auszuhandeln und gleichzeitig eine gute Qualität des Endprodukts sicherzustellen.
     * **Montage und Prüfung**  
     Auch die Herstellung und Testung von den Produkten gehört zu unseren Dienstleistungen.
- **VLSI-Design**  
Dank unserer langjährigen Erfahrung, die wir bei der Arbeit an unserem freien und Open-Source Halbleiterherstellungsprozess [LibreSilicon](https://libresilicon.com) gesammelt haben, können wir Sie von Ihrer Projektidee bis hin zu Ihrem Chip auf einer Leiterplatte begleiten und unterstützen:  
     + **Digitale Designs**  
     Unsere Entwickler sind erfahren im Umgang mit Verilog und dem FOSS-Tool [OpenLane](https://github.com/The-OpenROAD-Project/OpenLane), das bereits zahlreiche siliziumverifizierte Designs hervorgebracht hat und den Test der Zeit bestanden hat.
     + **Analoge Designs**  
     Mit [Magic](http://openCircuitdesign.com/magic), [KLayout](https://www.klayout.de), [gEDA](http://www.geda-project.org) und [ngspice ](https://ngspice.sourceforge.io) können wir VLSI-Analogdesigns für Sie entwerfen und simulieren.
     + **Tapeout**  
     Aufgrund unseres Netzwerks in der Branche können wir Ihnen dabei helfen, über unsere Partner Zugang zu günstigeren Tapeout Shuttles für Ihre Prototypen zu erhalten.
     + **Prozessdesign und -entwicklung**  
     Aufgrund unserer Erfahrung mit der Entwicklung von LibreSilicon können wir Sie beim Entwerfen, Implementieren und Testen Ihres eigenen neuen Prozesses für Ihren eigenen Fabrikaufbau unterstützen, indem wir unseren Test-Wafer-Generator [Danube River](https://gitlab.libresilicon.com/generator-tools) verwenden und die Standardzellen mit unserem Generator [Cell Generator](https://pdk.libresilicon.com) generieren.
