{% include 'top.tpl' %}

<div>
    <div class="mask p-3" style="background-color: rgba(0, 0, 0, 0.6);">
        <div class="d-flex justify-content-center align-items-center h-100 p-3">
            <div class="w-50 text-white">
                <h1 class="mb-3">{{contact}}</h1>
                <b class="mb-3">{{contactUs}}</b><br/>
                <div class="mb-3">
                    <b>WhatsApp</b><br/>
                    +351 935 424 092<br/>
                    <b>E-Mail</b><br/>
                    <a href="mailto:david.lanzendoerfer@lanceville.pt">david.lanzendoerfer@lanceville.pt</a>
                </div>
            </div>
        </div>
    </div>
</div>

{% include 'bottom.tpl' %}
