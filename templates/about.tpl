{% include 'top.tpl' %}

<div>
    <div class="mask p-3" style="background-color: rgba(0, 0, 0, 0.6);">
        <div class="d-flex justify-content-center align-items-center h-100 p-3">
            <div class="w-50 text-white">
                <h1 class="mb-3">{{about}}</h1>
                <div class="mb-3">
                    {{ content }}
                </div>
            </div>
        </div>
    </div>
</div>

{% include 'bottom.tpl' %}
