<!doctype html>
<html lang="{{lang}}">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.css" integrity="sha384-apgwmwKRpy7AYJWhW5tiFM0F8egDkpneD3Jrkj66VVFOvuLaWhMJqByfz1QEUdxU" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="/css/languages.min.css"/>
        <link rel="stylesheet" type="text/css" href="/css/lanceville.css"/>
        <link rel="stylesheet" type="text/css" href="/css/tailwind.css"/>
        <title>Lanceville Technology</title>
    </head>
    <body>
        <div class="overflow-hidden w-full h-full relative">
            <div class="flex h-full flex-1 flex-col">
                {% include 'header.tpl' %}
                    <main class="h-full w-full transition-width flex flex-col overflow-auto items-stretch flex-1 p-3" style="background: rgb(104,104,130); background: linear-gradient(0deg, rgba(104,104,130,1) 12%, rgba(255,255,255,1) 71%, rgba(104,104,130,1) 100%);">
