{% include 'top.tpl' %}

<div>
    <div class="mask p-3" style="background-color: rgba(0, 0, 0, 0.6);">
        <div class="d-flex justify-content-center align-items-center h-100 p-3">
            <div class="w-50 text-white">
                <h1 class="mb-3">{{title}}</h1>
                <h3 class="mb-3">{{homeHeading}}</h3>
                <h5 class="mb-3">{{homeSubtitle}}</h5>
            </div>
        </div>
    </div>
</div>

<div class="c-wrapper">
    <div id="carouselHome" class="carousel slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
            <button type="button" data-bs-target="#carouselHome" data-bs-slide-to="0" class="active" aria-current="true" aria-label="{{carouselTitle1}}"></button>
            <button type="button" data-bs-target="#carouselHome" data-bs-slide-to="1" aria-label="{{carouselTitle2}}"></button>
            <button type="button" data-bs-target="#carouselHome" data-bs-slide-to="2" aria-label="{{carouselTitle3}}"></button>
            <button type="button" data-bs-target="#carouselHome" data-bs-slide-to="3" aria-label="{{carouselTitle3}}"></button>
            <button type="button" data-bs-target="#carouselHome" data-bs-slide-to="4" aria-label="{{carouselTitle4}}"></button>
        </div>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <div class="carousel-caption">
                    <b>{{carouselTitle1}}</b>
                    <p>{{carouselAlt1}}</p>
                </div>
                <img src="/img/carousel/pcb.png" alt="{{carouselAlt1}}">
            </div>
            <div class="carousel-item">
                <div class="carousel-caption">
                    <b>{{carouselTitle2}}</b>
                    <p>{{carouselAlt2}}</p>
                </div>
                <img src="/img/carousel/kicp.png" alt="{{carouselAlt2}}">
            </div>
            <div class="carousel-item">
                <div class="carousel-caption">
                    <b>{{carouselTitle3}}</b>
                    <p>{{carouselAlt3}}</p>
                </div>
                <img src="/img/carousel/funmetas1.png" alt="{{carouselAlt3}}">
            </div>
            <div class="carousel-item">
                <div class="carousel-caption">
                    <b>{{carouselTitle3}}</b>
                    <p>{{carouselAlt3}}</p>
                </div>
                <img src="/img/carousel/funmetas2.png" alt="{{carouselAlt3}}">
            </div>
            <div class="carousel-item">
                <div class="carousel-caption">
                    <b>{{carouselTitle4}}</b>
                    <p>{{carouselAlt4}}</p>
                </div>
                <img src="/img/carousel/improv.png" alt="{{carouselAlt4}}">
            </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselHome" data-bs-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselHome" data-bs-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
        </button>
    </div>
</div>

{% include 'bottom.tpl' %}
