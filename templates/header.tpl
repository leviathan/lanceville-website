<header class="sticky-top">
    <div class="inner">
        <nav class="navbar navbar-expand-lg py-3 navbar-dark bg-dark shadow-sm ">
            <div class="container">
                <a href="/index_{{lang}}.html" class="navbar-brand">
                    <!-- Logo Image -->
                    <img src="img/lanceville.png" width="60" alt="" class="d-inline-block align-middle mr-2">
                    <!-- Logo Text -->
                    <span class="text-uppercase font-weight-bold">Lanceville Technology</span>
                </a>

                <button type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div id="navbarSupportedContent" class="collapse navbar-collapse">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active"><a href="/index_{{lang}}.html" class="nav-link">{{home}}</a></li>
                        <li class="nav-item"><a href="/services_{{lang}}.html" class="nav-link">{{services}}</a></li>
                        <li class="nav-item"><a href="/about_{{lang}}.html" class="nav-link">{{about}}</a></li>
                        <li class="nav-item"><a href="/contact_{{lang}}.html" class="nav-link">{{contact}}</a></li>
                    </ul>
                </div>
                <div id="navbarSupportedContent" class="collapse navbar-collapse">
                    <div class="btn-group dropdown navbar-nav ml-auto">
                        <button type="button" class="btn btn-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <span class="lang-sm lang-lbl" lang="{{lang}}"></span>
                        </button>
                        <ul class="dropdown-menu shadow w-220px" data-bs-theme="dark" role="menu">
                            <li><a href="index_de.html" class="lang-sm lang-lbl dropdown-item d-flex gap-2 align-items-center" lang="de"></a></li>
                            <li><a href="index_en.html" class="lang-sm lang-lbl dropdown-item d-flex gap-2 align-items-center" lang="en"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</header>
