                </main>
                <footer class="sticky-bottom bg-dark">
                    <p>Copyright Lanceville Technology 2023</p>
                </footer>
            </div>
        </div>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha384-wsqsSADZR1YRBEZ4/kKHNSmU+aX8ojbnKUMN4RyD3jDkxw5mHtoe2z/T/n4l56U/" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/@popperjs/core@2.11.8/dist/umd/popper.js" integrity="sha384-yBknSWNrSUPkBtbhhCJ07i/BOmbrigRhLKPzTAny+TT4uGAwIdfNTAkBd/3VzXbg" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.js" integrity="sha384-QxrwoUH4d7Wrt+fB3AlGg3JwZcpBYbMryqlOhMweIAEgZvT99mmlLSWgM5heOYx1" crossorigin="anonymous"></script>
    </body>
</html>
