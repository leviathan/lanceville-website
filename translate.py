#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from jinja2 import Environment, FileSystemLoader
from jinja2_markdown import MarkdownExtension
import json
import codecs
import markdown

PATH = os.path.dirname(os.path.abspath(__file__))

languages = [False, 'en','de']
pages = ["index", "services", "about", "contact"]
markups = ["services", "about"]

def render_template(jinja_env, template_filename, language, content):
    try:
        json_content = json.load(fp=open("lang/"+language+".json"))
    except Exception as e:
        print("Error rendering language: "+language)
        print(e)
        return None 
    else:
        if content:
            json_content['content'] = content
        return jinja_env.get_template(template_filename).render(json_content)

def create_index_html(jinja_env):
    mdroot = os.path.join(PATH, 'markdowns')
    for fname_root in pages:
        for lang in languages:
            fname = fname_root
            if lang:
                fname+='_'
                fname+=lang
            else:
                lang='en'
            fname+=".html"
            # rendering
            content = None
            if fname_root in markups:
                mdpath = os.path.join(mdroot, lang)
                mdfile = os.path.join(mdpath, fname_root)+".md"
                with open(mdfile, 'r') as f:
                    tempMd= f.read()
                    f.close()
                content = markdown.markdown(tempMd)
            with codecs.open(fname, 'w', "utf-8-sig") as f:
                html = render_template(jinja_env, fname_root+'.tpl', lang, content)
                if(html is not None):
                    f.write(html)

def main():
    #markdown2html()
    jinja_env = Environment(
        autoescape = False,
        loader=FileSystemLoader(os.path.join(PATH, 'templates')),
        trim_blocks = False,
        extensions=['jinja2_markdown.MarkdownExtension']
    )
    jinja_env.add_extension(MarkdownExtension)
    create_index_html(jinja_env)

########################################

if __name__ == "__main__":
    main()


